/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 20:02:28 by mfrankev          #+#    #+#             */
/*   Updated: 2016/11/25 20:02:29 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtoupper(char* str)
{
    char *ret;

    if (!(ret = (char*)malloc(ft_strlen(str) + 1)))
        return (NULL);
    while (str)
    {
        *ret = ft_toupper(*str);
        ret++;
        str++;
    }
    return(str);
}